const tddots = Array.from(document.querySelectorAll('.tddots'))

tddots.forEach((dot) => {
  dot.addEventListener('click', () => {
    console.log(dot)
    const tddotsHelp = dot.querySelector('.tddots__help')
    const circles = Array.from(dot.querySelectorAll('circle'))

    hideHelps()

    circles.forEach((circle) => {
      circle.style.fill = '#2469F0'
      circle.style.opacity = 1
    })

    tddotsHelp.style.display = 'block'
    console.log(tddotsHelp.style.display)
  })
})

function hideHelps() {
  const tddotsHelps = Array.from(document.querySelectorAll('.tddots__help'))
  const circles = Array.from(document.querySelectorAll('circle'))

  tddotsHelps.forEach((item) => {
    item.style.display = 'none'
    console.log(item)
  })

  circles.forEach((circle) => {
    circle.style.fill = '#1D1D1F'
    circle.style.opacity = 0.4
  })
}
